package game.android.our.ourgame.Utilities;

import android.app.Application;

import com.orhanobut.hawk.Hawk;

public class myApplication extends Application {
    public static myApplication mContext;

    @Override
    public void onCreate() {
        super.onCreate();

        mContext = this;
        Hawk.init(this).build();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
