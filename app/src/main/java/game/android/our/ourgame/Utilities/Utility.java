package game.android.our.ourgame.Utilities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import static android.content.Context.CONNECTIVITY_SERVICE;

import game.android.our.ourgame.R;

public class Utility {

    public static ProgressDialog dialog;

    public static void initialDialog(Context mContext) {
        dialog = new ProgressDialog(mContext);
        dialog.setTitle(" کمی صبر کنید");
        dialog.setMessage("بارگذاری اطلاعات از سامانه ....");
    }

    //get permission from client
    public static void getPermission(Activity activity) {
        Dexter.withActivity(activity)
                .withPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA,
                        Manifest.permission.GET_ACCOUNTS,
                        Manifest.permission.READ_CONTACTS


                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
            }
        }).check();
    }

    //check the cell have internet or not
    public static boolean checkInternet() {
        ConnectivityManager con_manager = (ConnectivityManager) myApplication.mContext.getSystemService(CONNECTIVITY_SERVICE);

        if (con_manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                con_manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            return true;
        } else
            return false;

    }


    //show a dialog to turnON internet
    public static Dialog InternetDialogBox(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(myApplication.mContext);

        builder.setTitle("هشدار");
        builder.setIcon(R.drawable.ic_launcher_background);
        builder.setMessage("لطفا یکی از این موارد را جهت اتصال به اینترنت انتخاب نمائید");


        // Add the buttons
        builder.setPositiveButton("وایفا", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                myApplication.mContext.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));

            }
        });
        builder.setNegativeButton("داده های موبایل", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                myApplication.mContext.startActivity(new Intent(Settings.ACTION_SETTINGS));
            }
        });

        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Toast.makeText(myApplication.mContext, "شما باید یکی از موارد را انتخاب نمائید", Toast.LENGTH_SHORT).show();
                InternetDialogBox(activity).show();

            }
        });

        return builder.create();

    }


    // Create the LONG Toast for the message
    public static void longToast(String msg) {
        Toast.makeText(myApplication.mContext, msg, Toast.LENGTH_LONG).show();
    }

    // Create the SHORT Toast for the message
    public static void shortToast(String msg) {
        Toast.makeText(myApplication.mContext, msg, Toast.LENGTH_SHORT).show();
    }

}
